let fs = require('fs');
let JSZip = require('JSZip');
// let Docxtemplater = require('Docxtemplater');
let mammoth = require('mammoth');
let cheerio = require('cheerio');
let PDFParser = require("pdf2json");
let pdftohtml = require('pdftohtmljs');
let data = {};

console.log(`TIME STARTED`, new Date().toString());

const onFileContent = (filename, content) => {
    console.log('Reading filename', filename);
    console.log('Reading content', content);
    data[filename] = content;
}

const onError = (filename, err) => {
    console.log(`ERROR ENCOUNTERED reading file ${filename}: `, err);
    return false;
}





const readFiles = (dirname, onFileContent, onError) => {
    const filenames = fs.readdirSync(dirname);
    filenames.forEach((filename) => {
        const isShadowFile = filename.indexOf('~$') > -1 ? true : false;
        if (isShadowFile) {
            return;
        }


        const fullFilePath = `${dirname}${filename}`;
        const fullNewFilePath = `./html/${filename}.html`;


        let pdfParser = new PDFParser();

        pdfParser.on("pdfParser_dataError", errData => console.error(errData.parserError));
        pdfParser.on("pdfParser_dataReady", pdfData => {
            fs.writeFile( fullNewFilePath, JSON.stringify(pdfParser.getAllFieldsTypes));
        });

        pdfParser.loadPDF( fullFilePath);


    });
}


readFiles('./pdf/', onFileContent, onError);


console.log(`TIME ENDED`, new Date().toString());
