let fs = require('fs');
let JSZip = require('JSZip');
let Docxtemplater = require('Docxtemplater');
let mammoth = require('mammoth');
let cheerio = require('cheerio');

let json2xls = require('json2xls');
let data = {};
let $, dataList = [];
let generatedFileName = './xls/Generated-'
console.log(`TIME STARTED`, new Date().toString());

const onFileContent = (filename, content) => {
    console.log('Reading filename', filename);
    console.log('Reading content', content);
    data[filename] = content;
}

const onError = (filename, err) => {
    console.log(`ERROR ENCOUNTERED reading file ${filename}: `, err);
    return false;
}
const sanitizeData = (htmlChunk) => {
    return $(`<p>${htmlChunk}</p>`).text() || '';
}

const readFiles = (dirname, onFileContent, onError) => {
    const filenames = fs.readdirSync(dirname);
    filenames.forEach((filename) => {
        const isShadowFile = filename.indexOf('~$') > -1 ? true : false;
        if (isShadowFile) {
            return;
        }
        const fullFilePath = `${dirname}${filename}`;
        const dataInputHtml = fs.readFileSync(fullFilePath, 'utf-8');

        $ = cheerio.load(dataInputHtml);
        const cvItem = {};
        cvItem['fileName'] =  (filename).replace('.html', ''); 
        cvItem['filePath'] =  __dirname.replace('html', 'docx') +  '\\doc\\'+ cvItem['fileName'] ; 
        cvItem['candidateName'] = sanitizeData($('table:nth-child(1) tr td:nth-child(2)').html());
        cvItem['cvReceived'] = sanitizeData($('table:nth-child(1) tr td:nth-child(3)').html());


        if ($('table:nth-child(2)')) {
            for (let i = 1; i <= $('table:nth-child(2) tr').length; i++) {
                const keyName = sanitizeData($(`table:nth-child(2) tr:nth-child(${i}) td:nth-child(1)`).html());
                const keyValue = sanitizeData($(`table:nth-child(2) tr:nth-child(${i}) td:nth-child(2)`).html());
                cvItem[keyName] = (keyValue);
            }
        }

        if ($('table:nth-child(3)')) {
            for (let i = 1; i <= $('table:nth-child(3) tr').length; i++) {
                const keyName = sanitizeData($(`table:nth-child(3) tr:nth-child(${i}) td:nth-child(1)`).html());
                const keyValue = sanitizeData($(`table:nth-child(3) tr:nth-child(${i}) td:nth-child(2)`).html());
                cvItem[keyName] = (keyValue);
            }
        }



        if ($('table:nth-child(4)')) {
            for (let i = 1; i <= $('table:nth-child(4) tr').length; i++) {
                const keyName = sanitizeData($(`table:nth-child(4) tr:nth-child(${i}) td:nth-child(1)`).html());
                const keyValue = sanitizeData($(`table:nth-child(4) tr:nth-child(${i}) td:nth-child(2)`).html());
                cvItem[keyName] = (keyValue);
            }
        }

        //misc info
        if ($('body')) {
            const beginMiscIndexPos = dataInputHtml.lastIndexOf("</table>") || 0;
            cvItem['Free Format'] = dataInputHtml.substring(beginMiscIndexPos, dataInputHtml.length) || '';
            cvItem['Free Format'] = cvItem['Free Format'].replace('</table>', '')
        } 
        dataList.push(cvItem);



        console.log('\n');
        console.log('cvItem ', (cvItem));


    });

    let xls = json2xls(dataList);

    fs.writeFileSync(`${generatedFileName}${new Date().getTime().toString()}.xls`, xls, 'binary');

}


readFiles('./html/', onFileContent, onError);


console.log(`TIME ENDED`, new Date().toString());
