let fs = require('fs');
let JSZip = require('JSZip');
// let Docxtemplater = require('Docxtemplater');
let mammoth = require('mammoth');
let cheerio = require('cheerio');

let data = {};

console.log(`TIME STARTED`, new Date().toString());

const onFileContent = (filename, content) => {
    console.log('Reading filename', filename);
    console.log('Reading content', content);
    data[filename] = content;
}

const onError = (filename, err) => {
    console.log(`ERROR ENCOUNTERED reading file ${filename}: `, err);
    return false;
}

const readFiles = (dirname, onFileContent, onError) => {
    const filenames = fs.readdirSync(dirname);
    filenames.forEach((filename) => {
        const isShadowFile = filename.indexOf('~$') > -1 ? true : false;
        if (isShadowFile) {
            return;
        }



        const directoryPrefix = dirname.replace(/\./ig, '').replace(/\//ig, '').replace(/\\/ig, '') || 'docx';
        // console.log(directoryPrefix + ' is directoryPrefix');
        const fullFilePath = `${dirname}${filename}`;
        const fullNewFilePath = `./html/${directoryPrefix}-${filename}.html`;


        stat = fs.statSync(fullFilePath)
        if (stat.isDirectory()) {
            console.log(fullFilePath + ' is directory');
            readFiles(fullFilePath + '/', onFileContent, onError);
            return;
        }


        mammoth.convertToHtml({ path: fullFilePath })
            .then(function (result) {

                console.log(`fullNewFilePath ${fullNewFilePath} ....`);
                let html = result.value; // The generated HTML  
                // console.log( html);
                fs.writeFileSync(`${fullNewFilePath}`, html);


            }).done();



    });
}


readFiles('./docx/', onFileContent, onError);


console.log(`TIME ENDED`, new Date().toString());
