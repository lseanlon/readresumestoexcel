const express = require('express')
const app = express()

require('shelljs/global');

const docxToXls = () => {
  const version = exec('node ConvertDocxToHtml.js && node ConvertHtmlToXls.js', { silent: true }).output;
}

app.get('/DocxToXls', function (req, res) {

  docxToXls();
  res.send('Completed DocxToXls');


})

app.get('/DocToXls', function (req, res) {




  let spawn = require("child_process").spawn, child;
  child = spawn("C:\\Windows\\SysWOW64\\WindowsPowerShell\\v1.0\\powershell.exe", [".\\ConvertDocToDocx.ps1"]);
  child.stdout.on("data", function (data) {
    console.log("Powershell Data: " + data);
  });
  child.stderr.on("data", function (data) {
    console.log("Powershell Errors: " + data);
  });
  child.on("exit", function () {
    console.log("Powershell Script finished");
  });
  child.stdin.end();


  docxToXls();
  res.send('DocToXls  Done - result ')
})



app.listen(3000, function () {
  console.log('App listening on port 3000!')
})