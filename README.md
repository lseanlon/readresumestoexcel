
 ## DOC PARSER ##

## Setup Installation ##
    1) Run powershell as admin [both sys32 and sys64]
    Set-ExecutionPolicy RemoteSigned

    2) npm install

    3) Setup environment variable so powershell in path [both sys32 and sys64]
        C:\Windows;
        C:\Windows\system32;
        C:\Windows\System32\Wbem;
        C:\Windows\System32\WindowsPowerShell\v1.0\;
        C:\Windows\SysWOW64\WindowsPowerShell\v1.0\powershell.exe

## Use Case ##
    1) How i convert the cv lists of '.doc' to aggregated '.xls'
    - Place all the doc into /doc folder
    - Open powershell, run ConvertDocToDocx.ps1
    - Run node ConvertDocxToHtml.js && node ConvertHtmlToXls.js
    - Open /xls folder. Latest generated file
    - ... Or run service localhost:3000/docToXls


    2) How i convert the cv lists of '.docx' to aggregated '.xls'
    - Place all the docx into /docx folder
    - Run  node ConvertDocxToHtml.js && node ConvertHtmlToXls.js
    - Open /xls folder. Latest generated file
    - ... Or run service localhost:3000/docxToXls


    3) How i convert the cv lists of '.pdf' to aggregated '.xls'
    - Convert online pdf to docx  
    - Follow step2
 

## RUN FUNCTIONS ##
    1) Convert all raw files '.doc' folder to '.docx'  /powershell
    ./ConvertDocToDocx.ps1


    2) Read all '.docx' content into html, parse with cheerio, parse into xls
    node ConvertDocxToHtml.js && node ConvertHtmlToXls.js


    3) Read all '.pdf' content into html, parse with cheerio, parse into xls
    node ConvertPdfToHtml.js && node ConvertPdfToXls.js
 

## RUN SERVICES ##
    1) node app.js

    http://localhost:3000/DocToCsv
    http://localhost:3000/DocxToCsv
    http://localhost:3000/PdfToCsv


    2) Ip access
    ifconfig \all 
    ipconfig 