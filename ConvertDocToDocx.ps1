[Reflection.Assembly]::LoadWithPartialName("Microsoft.Office.Interop.Word") | Out-Null

[ref]$SaveFormat = "microsoft.office.interop.word.WdSaveFormat" -as [type] 
$word = New-Object -ComObject word.application 
$word.visible = $false 
$folderpath = "D:\workspace\readresumestoexcel\doc\*"  
$outputpath = "D:\workspace\readresumestoexcel\docx\" 

$fileType = "*doc" 
$childItems = Get-ChildItem -recurse  -path $folderpath -include $fileType 

$childItems | foreach-object {  

if ( $_.name.lastindexOf("$") -lt 0) {  
        Write-Host $_.FullName -foregroundcolor cyan 

        $path = ($_.fullname).substring(0,($_.FullName).lastindexOf("."))  
        $newPath =  ($_.FullName).replace('D:\workspace\readresumestoexcel\doc\', '-') 
        $newPath =  ($newPath).replace('\', '').replace('\\', '') 

       # Write-Host $path -foregroundcolor cyan 
        Write-Host $folderPath -foregroundcolor cyan 
        Write-Host newPath ($_.fullname + $newPath) -foregroundcolor cyan 
        #  "newPath  $newPath ..." 
        "Converting ( $_.name ) to $fileType ..." 
           $doc = $word.documents.open($_.fullname) 
          $doc.saveas([ref] $outputpath , [ref]$SaveFormat::wdFormatHTML) 
          $doc.close() 
        Rename-Item -Path  $_.fullname -NewName  ($_.fullname + $newPath)
    }

} 
    $word.Quit() 
    $word = $null 


"Copying from $folderpath to $outputpath ..." 
Copy-Item -path $folderpath -include "*.docx" -Destination $outputpath

[gc]::collect() 
[gc]::WaitForPendingFinalizers()
